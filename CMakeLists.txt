cmake_minimum_required (VERSION 2.8)

# Tell CMake where to look for the compiler. 

# Project Info
project(CGenericList)
set(VERSION_MAJOR 0)
set(VERSION_MINOR 0)
set(VERSION_PATCH 0)
set(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

message("Binary Tree Path: ${PROJECT_BINARY_DIR}")
message("CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}")

if(APPLE)
	message("Apple system detected.")
elseif(WIN32)
	message("Windows 32bit system detected.")
elseif(UNIX)
	message("Unix system detected.")
endif()
message("${CMAKE_SYSTEM} is currently being used.")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -ansi -std=c11 -Wpedantic -pedantic-errors -w -Wextra -Wall")

add_subdirectory(src)
add_subdirectory(test)

