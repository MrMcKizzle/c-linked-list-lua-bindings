project(CGenericLibrary)

file( GLOB SRCS "list.c" )

add_library(lib_generic_list ${SRCS})

target_include_directories(lib_generic_list PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})


