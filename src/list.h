#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

/**
 * Nodes represent data that is going to be inserted into the list.  
 * They also contain pointers to the next and previous items in the list. 
 */
typedef struct node Node;
struct node {
	size_t data_size;
	void * data;
	Node * next;
	Node * previous;
};

/**
 * Functions that can act on the list item. 
 */
Node * newNode(void * data, size_t size);

/**
 * A List struct stores a pointer to the head and tail of the list. 
 */
typedef struct list List;
struct list{
	unsigned int count;
	Node * head;
	Node * tail;
};

/**
 * Functions that can act on the list. 
 */
void hellolist();
List * newList();
void deleteList(List * list);

#endif


