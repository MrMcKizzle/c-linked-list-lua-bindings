#include <stdlib.h>
#include <stdio.h>

#include "list.h"

void hellolist() {
	printf("Hello from the generic list library!!!");
}

/**
 * Initialize a new item with a given dataset. 
 */
Node * newNode(void * data, size_t size) {
	Node * item = (Node *)calloc(1, size);

	return item;
}

/**
 * Deletes an item in a safe fasion. Makes sure that the next and previous
 * items are properly linked together. Will not delete the dataset within the 
 * list item. 
 */
void deleteListItem(Node * node) {
	return;	
}

/**
 * Creates a new list.
 */
List * newList() {
	return NULL;
}

/**
 * Deletes a list without deleting the contained data. 
 */
void deleteList(List * list) {
	return;
}


