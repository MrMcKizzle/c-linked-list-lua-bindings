project(GenericListTests)

file( GLOB SRCS "main.c")

add_executable(clist_test ${SRCS})

target_include_directories(clist_test PRIVATE /usr/local/include /usr/include)
target_link_libraries(clist_test PUBLIC lib_generic_list lua.5.3 lua5.2)

